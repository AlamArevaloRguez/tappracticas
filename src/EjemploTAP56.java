
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ALAM
 */
public class EjemploTAP56 extends JFrame{
    public static void main(String[] args) {
        EjemploTAP56 instancia = new EjemploTAP56();
        instancia.run();
      
    }
    private void run(){
        
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        ventana.setSize(400,300);
        
        
        JPanel panel = new JPanel();
        BoxLayout boxly = new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS);
        
        this.setLayout(boxly);
        
        panel.setBorder(new EmptyBorder(new Insets(150, 200, 150, 200)));
        
        JLabel etiqueta = new JLabel("Hola mundo");
        JButton boton = new JButton("Cerrar");
        
        boton.addActionListener(new MiControlador());
        
        panel.add(etiqueta);
        panel.add(boton);
        this.add(panel);
        this.pack();
        
        this.add(panel);
        
        this.setVisible(true);
    }
}

class MiControlador implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(null, "Hola mundo, otra vez!. Presionaron el  boton cerrar");
    }
    
}